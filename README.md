# golang-kafka

Wrapper of [sarama](https://github.com/Shopify/sarama)

## Getting Started

### kafka init

### download

https://kafka.apache.org/downloads
Binary downloads

### start apache Kafka and zookeeper

zookeeper

```
@echo off
cd /D C:\kafka_2.12-2.8.0\bin\windows
call zookeeper-server-start.bat ..\..\config\zookeeper.properties
```

apache Kafka

```
@echo off
cd /D c:\kafka_2.12-2.8.0\bin\windows
call kafka-server-start.bat ..\..\config\server.properties
```

### create topic

```
kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 2 --topic golang-kafka

kafka-topics.bat --list --zookeeper localhost:2181
golang-kafka
```
