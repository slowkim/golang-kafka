package kafka

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"io/ioutil"
	"math/rand"
	"testing"

	"github.com/Shopify/sarama"
	"github.com/stretchr/testify/assert"
)

var (
	brokers      = []string{"localhost:9092"}
	topic        = "golang-kafka"
	messageCount = 100
)

func TestPubSub(t *testing.T) {
	produceMessages := map[int]interface{}{}
	consumeMessages := map[int]interface{}{}

	producer, err := NewProducer(brokers, topic, func(c *sarama.Config) {
		if tlsConfig, err := createTlsConfiguration(); err == nil {
			c.Net.TLS.Config = tlsConfig
			c.Net.TLS.Enable = true
		}
	})
	assert.Equal(t, nil, err)

	consumer, err := NewConsumer(brokers, topic, nil, sarama.OffsetNewest, func(c *sarama.Config) {
		if tlsConfig, err := createTlsConfiguration(); err == nil {
			c.Net.TLS.Config = tlsConfig
			c.Net.TLS.Enable = true
		}
	})
	assert.Equal(t, nil, err)

	messages, err := consumer.Messages()
	assert.Equal(t, nil, err)

	closing := make(chan struct{})
	i := 0
	go func() {
		for m := range messages {
			var v map[string]interface{}
			d := json.NewDecoder(bytes.NewReader(m.Value))
			d.UseNumber()
			err := d.Decode(&v)
			assert.Equal(t, nil, err)

			t.Logf("[Receive] Offset:%d\tPartition:%d\tValue:%v\n", m.Offset, m.Partition, v)
			idx, err := v["idx"].(json.Number).Int64()
			assert.Equal(t, nil, err)
			msg, err := v["msg"].(json.Number).Int64()
			assert.Equal(t, nil, err)
			consumeMessages[int(idx)] = int(msg)
			i++
			if i == messageCount {
				break
			}
		}
		consumer.Close()
		closing <- struct{}{}
	}()

	// run producer
	for i := 0; i < messageCount; i++ {
		msg := map[string]interface{}{
			"idx": i,
			"msg": rand.Int(),
		}
		err := producer.Send(msg)
		//assert.L
		assert.Equal(t, nil, err)
		//t.Log("[Send]", msg)
		produceMessages[i] = msg["msg"]
	}

	<-closing
	t.Log("closed")

	for k, v := range produceMessages {
		m, ok := consumeMessages[k]
		if !ok {
			t.Fail()
		}
		assert.Equal(t, v, m)
	}
}

func createTlsConfiguration() (*tls.Config, error) {
	cert, err := tls.LoadX509KeyPair("cert.crt", "cert.key")
	if err != nil {
		return nil, err
	}

	caCert, err := ioutil.ReadFile("cert.ca")
	if err != nil {
		return nil, err
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	return &tls.Config{
		Certificates:       []tls.Certificate{cert},
		RootCAs:            caCertPool,
		InsecureSkipVerify: true,
	}, nil
}
